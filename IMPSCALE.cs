﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace IMPSCALE
{
    public class IMPSCALE : AOPluginEntry
    {
        string pdir;
        string[] EmptyImplants = {
                                "Basic Chest Implant" ,
                                "Basic Ear Implant" ,
                                "Basic Eye Implant" ,
                                "Basic Feet Implant" ,
                                "Basic Head Implant" ,
                                "Basic Left Arm Implant" ,
                                "Basic Left Hand Implant" ,
                                "Basic Left Wrist Implant" ,
                                "Basic Leg Implant" ,
                                 "Basic Right Arm Implant" ,
                                "Basic Right Hand Implant" ,
                                "Basic Right Wrist Implant" ,
                                "Basic Waist Implant"

        };

        string[] FilledImplants = {
                                "Chest Implant:" ,
                                "Ear Implant:" ,
                                "Eye Implant:" ,
                                "Feet Implant:" ,
                                "Head Implant:" ,
                                "Left Arm Implant:" ,
                                "Left Hand Implant:" ,
                                "Left Wrist Implant:",
                                "Leg Implant:",
                                "Right Arm Implant:" ,
                                "Right Hand Implant:" ,
                                "Right Wrist Implant:" ,
                                "Waist Implant:"

        };


        byte[] payload = null;
        Window wind;
        bool constructing = false;

        public override void Run(string pluginDir)
        {
            Chat.WriteLine("Impscale loaded");

            pdir = pluginDir;
            Chat.RegisterCommand("imp", (string command, string[] param, ChatWindow chatWindow) =>
            {
                wind = Window.CreateFromXml("Trade Repeater", Path.Combine(Path.Combine(pdir, "UI"), "form.xml"));
                wind.ResizeTo(new Vector2(170, 150));

                wind.FindView("buttonSend", out Button but);
                but.Clicked += butClicked;

                wind.FindView("chkConstructing", out Checkbox chk);
                chk.Toggled += chkToggled;

                chk.SetValue(constructing);

                wind.FindView("tvitem", out TextView tv);
                if (payload != null)
                {
                    tv.Text = "Item: DETECTED";
                }

                else
                    tv.Text = "Item:";
                wind.Show(true);

            });


            //Chat.RegisterCommand("op", (string command, string[] param, ChatWindow chatWindow) =>
            //{

            //    if (payload != null)
            //        Network.Send(payload);


            //});



            //Network.N3MessageReceived += N3MessageReceived;

            //int numberOfDisassembleClinics = 0;
            //int numberOfImplants = 0;

            //foreach (Item it in Inventory.Items.Where(n => n.Slot.Type == IdentityType.Inventory))
            //{

            //    if (it.Name == "Implant Disassembly Clinic")
            //    {
            //        numberOfDisassembleClinics++;
            //        disassemblyclinictool = it;
            //    }
            //    else if (
            //                        EmptyImplants.Contains(it.Name) ||
            //                        it.Name.StartsWith("Chest Implant:") ||
            //                        it.Name.StartsWith("Ear Implant:") ||
            //                        it.Name.StartsWith("Eye Implant:") ||
            //                        it.Name.StartsWith("Feet Implant:") ||
            //                        it.Name.StartsWith("Head Implant:") ||
            //                        it.Name.StartsWith("Feet Implant:") ||
            //                        it.Name.StartsWith("Left Arm Implant:") ||
            //                        it.Name.StartsWith("Left Hand Implant:") ||
            //                        it.Name.StartsWith("Left Wrist Implant:") ||
            //                        it.Name.StartsWith("Leg Implant:") ||
            //                        it.Name.StartsWith("Waist Implant:") ||
            //                        it.Name.StartsWith("Right Arm Implant:") ||
            //                        it.Name.StartsWith("Right Hand Implant:") ||
            //                        it.Name.StartsWith("Right Wrist Implant:")
            //        )
            //        numberOfImplants++;

            //    //Built implants


            //}

            //if (numberOfDisassembleClinics != 1 || numberOfImplants != 1)
            //    {
            //        constructing = false;
            //        Chat.WriteLine("Halted: Verify inventory if number of disassemble clinics is one and number of implantes is one");
            //        return;
            //    }
            Network.N3MessageSent += N3MessageSent;


            Game.OnUpdate += OnUpdate;
        }

        private void chkToggled(object sender, bool e)
        {
            if (e == true)
            {

                int numberOfDisassembleClinics = 0;
                int numberOfImplants = 0;

                foreach (Item it in Inventory.Items.Where(n => n.Slot.Type == IdentityType.Inventory))
                {

                    if (it.Name == "Implant Disassembly Clinic")
                    {
                        numberOfDisassembleClinics++;
                        //disassemblyclinictool = it;
                    }
                    else if (
                                        EmptyImplants.Contains(it.Name) ||
                                        it.Name.StartsWith("Chest Implant:") ||
                                        it.Name.StartsWith("Ear Implant:") ||
                                        it.Name.StartsWith("Eye Implant:") ||
                                        it.Name.StartsWith("Feet Implant:") ||
                                        it.Name.StartsWith("Head Implant:") ||
                                        it.Name.StartsWith("Feet Implant:") ||
                                        it.Name.StartsWith("Left Arm Implant:") ||
                                        it.Name.StartsWith("Left Hand Implant:") ||
                                        it.Name.StartsWith("Left Wrist Implant:") ||
                                        it.Name.StartsWith("Leg Implant:") ||
                                        it.Name.StartsWith("Waist Implant:") ||
                                        it.Name.StartsWith("Right Arm Implant:") ||
                                        it.Name.StartsWith("Right Hand Implant:") ||
                                        it.Name.StartsWith("Right Wrist Implant:")
                        )
                        numberOfImplants++;

                    //Built implants


                }

                if (numberOfDisassembleClinics != 1 || numberOfImplants != 1)
                {
                    constructing = false;
                    Chat.WriteLine("Unbale to construct: Verify inventory if number of disassemble clinics is one and number of implantes is one");

                    ((Checkbox)sender).SetValue(false);
                    constructing = false;
                    return;
                }


            }
            constructing = e;
        }

        private void butClicked(object sender, ButtonBase e)
        {
            if (payload != null)
            {
                wind.FindView("tivqtd", out TextInputView tiv);

                if (int.TryParse(tiv.Text, out int qtd))
                {
                    for (int i = 1; i <= qtd; i++)
                    {
                        Network.Send(payload);

                    }

                }


            }

        }

        private void N3MessageSent(object sender, N3Message e)
        {
            //Chat.WriteLine(e.N3MessageType.ToString());


            //if (e.N3MessageType == N3MessageType.CharacterAction)
            //{
            //    CharacterActionMessage act = (CharacterActionMessage)e;
            //    //parm2 implant inventory slot
            //    //target cluster inventory slot

            //    List<Item> imps = Inventory.Items.Where
            //        (o => o.Slot.Type == IdentityType.Inventory && o.Name.StartsWith("Basic Ear Implant")).ToList();


            //}

            //if (e.N3MessageType == N3MessageType.GenericCmd)
            //{



            //}

            if (e.N3MessageType == N3MessageType.Trade)
            {
                TradeMessage msg = (TradeMessage)e;
                if (msg.Action == TradeAction.AddItem)
                {
                    payload = PacketFactory.Create(e);
                    if (wind != null)
                    {
                        if (wind.IsValid)
                        {
                            wind.FindView("tvitem", out TextView tv);
                            if (payload != null)
                                tv.Text = "Item: DETECTED";
                        }

                    }
                }
            }

        }

        //private void N3MessageReceived(object sender, N3Message e)
        //{

        //}

        private void OnUpdate(object sender, float e)
        {
            if (constructing)
            {
                ///////////////////////////////////////////////////////////////////////////////////////////////                
                //validations: NEEDED
                //                      ONE DISASSEMBLE

                //                      ONE IMPLANT (BASIC OR OTHER TYPE)
                ///////////////////////////////////////////////////////////////////////////////////////////////            
                ///



                //if (numberOfDisassembleClinics != 1 || numberOfImplants !=1)
                //{
                //    constructing = false;
                //    Chat.WriteLine("Halted: Verify inventory if number of disassemble clinics is one and number of implantes is one");
                //    return;
                //}


                if (Inventory.Items.Where(i => i.Slot.Type == IdentityType.Inventory && EmptyImplants.Any(s => s.StartsWith(i.Name))).FirstOrDefault() != null)
                {
                    //empty


                    Item clusteritem = Inventory.Items.Where(o => o.Slot.Type == IdentityType.Inventory && o.Name.Contains("Cluster - ")).FirstOrDefault();
                    Item implantitem = Inventory.Items.Where(i => i.Slot.Type == IdentityType.Inventory && EmptyImplants.Any(s => s.StartsWith(i.Name))).FirstOrDefault();
                    if (clusteritem != null)
                    {
                        CharacterActionMessage cam = new CharacterActionMessage();
                        cam.Action = CharacterActionType.UseItemOnItem;
                        cam.Identity = DynelManager.LocalPlayer.Identity;
                        cam.Parameter1 = 0x68;
                        cam.Parameter2 = implantitem.Slot.Instance;
                        cam.Target = clusteritem.Slot;
                        Network.Send(cam);
                    }


                }
                else
                {
                    //filled

                    Item disassemblytoolitem = Inventory.Items.Where(o => o.Name== "Implant Disassembly Clinic").FirstOrDefault();
                    
                    
                    Item implantitem = Inventory.Items.Where(
                                        i => i.Slot.Type == IdentityType.Inventory 
                                        && StartsWith(i.Name)).FirstOrDefault();


                    if (implantitem != null && disassemblytoolitem!=null)
                    {
                        CharacterActionMessage cam = new CharacterActionMessage();
                        cam.Action = CharacterActionType.UseItemOnItem;
                        cam.Identity = DynelManager.LocalPlayer.Identity;
                        cam.Parameter1 = 0x68;
                        cam.Parameter2 = implantitem.Slot.Instance;
                        cam.Target = disassemblytoolitem.Slot;
                        Network.Send(cam);
                    }

                }




            }
        }

        bool StartsWith(string name)
        {
            foreach (string s in FilledImplants)
            {
                if (name.StartsWith(s)) return true;
            }

            return false;
        }


    }
}

    

   

